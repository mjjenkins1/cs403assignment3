(include "pretty.lib")
(define (author)
	(println "AUTHOR: Melissa Jenkins mjjenkins1@crimson.ua.edu")
	)

;********************************************************************************************
;Problem 1
;********************************************************************************************
(define (run1)
  (define l 0)
  (inspect (scoping '* ((define (zurp x) (define y (+ x 1)) this) 5)))
  )
(define (scoping symbol function)
  (cond
	((pair? (assoc symbol (bindings (function))))
	 'bound
	 )
	((pair? (assoc symbol (bindings (dot function __context))))
	 'free
	 )
	((pair? (assoc symbol (bindings (dot (dot function __context) __context))))
	 'free
	 )
	((pair? (assoc symbol (bindings (dot (dot (dot function __context) __context) __context))))
	 'free
	 )
	((pair? (assoc symbol (bindings (dot (dot (dot (dot function __context) __context) __context) __context))))
	 'free
	 )
	(else
	  'undefined
	  )
	)
  )

;********************************************************************************************
;Problem 2
;********************************************************************************************
;{
(define (run2)
  	(define x 2)
	(define (square x) (multiply x x)(+ x 1))
	(define (multiply a b) (* a b))
	(pretty square)
	(pretty multiply)
	(inspect (define body (get 'code square)))
	(inspect (define denv (get '__context square)))
  )
;}
(define (compile function)
  (cond
    ((closure? function)
     (println "THIS IS A CLOSURE")
     )
    (else
      (println "THIS IS NOT A CLOSURE")
      )
    )
  )

;********************************************************************************************
;Problem 3
;********************************************************************************************
(define (run3)
  (define b (bst))
	(inspect ((b 'insert) 3 4 5 1 0 2 6))
	(inspect ((b 'find) 5))
	(inspect ((b 'find) 7))
	(inspect ((b 'root)))
	(inspect ((b 'size)))
	(inspect ((b 'traverse)))
	(inspect ((b 'delete) 3))
	(inspect ((b 'traverse)))
	)
(define (bst)
  (define bstRoot nil)
  (define bstSize 0)

  (define type 'bst)

  ;member functions
  (define (root)
	(cond
	  ((null? bstRoot) nil)
	  (else
		(car bstRoot)
		)
	  )
	)

  (define (size)
	bstSize
	)

  (define (traverse)
	(define (traverseHelper currValue)
	  (cond
		((eq? currValue nil) nil)
		((null? (cdr currValue)) (print (car currValue) " "))
		(else
		  (print (car currValue) " ")
		  (traverseHelper (car (cdr currValue)))
		  (traverseHelper (cdr (cdr currValue)))
		  )
		)
	  )
	(traverseHelper bstRoot)
		)
   
  (define (insert @)
	(set! @ (flatten @))
	;Helper method to insert individual values
	(define (insertHelper newValue currValue)
	  (cond
		((null? currValue) nil)
		((> newValue (car currValue))
		 (cond
		   ((null? (cdr currValue))
			(set-cdr! currValue (cons nil (cons newValue nil)))
			)
		   ((null? (cdr (cdr currValue)))
			(set-cdr! (cdr currValue) (cons newValue nil))
			)
		   (else
			 (insertHelper newValue (cdr (cdr currValue)))
			 )
		   )
		 )
		(else
		  (cond
			((null? (cdr currValue))
			 (set-cdr! currValue (cons (cons newValue nil) nil))
			 )
			((null? (car (cdr currValue)))
			 (set-car! (cdr currValue) (cons newValue nil))
			 )
			(else
			  (insertHelper newValue (car (cdr currValue)))
			  )
			)
		  )
		)
		)
	(cond
	  ((null? @) nil)
	  (else
	   		(cond 
	  			((null? bstRoot) 
		 			(set! bstRoot (cons (car @) nil))
					(set! bstSize (+ bstSize 1))
					(insert (cdr @))
		 			)
				(else
		  			(insertHelper (car @) bstRoot)
					(set! bstSize (+ bstSize 1))
					(insert (cdr @))
					)
				)
			)
	  )
	)
  (define (find val)
	(define (findHelper currVal)
	  (cond
		((null? currVal) #f)
		((eq? val (car currVal)) #t)
		((null? (cdr currVal)) #f)
		((> val (car currVal))
		 (findHelper (cdr (cdr currVal)))
		 )
		(else
		  (findHelper (car (cdr currVal)))
		  )
		)
	  )
	(findHelper bstRoot)
	)
  (define (findMinValue curr)
	 (cond
	   ((null? (cdr curr)) (car curr)) ;is leaf node
	   ((null? (car (cdr curr))) (car curr));has right child, but return value bc is min
	   (else
	     (findMinValue (car (cdr curr)))
	     )
	   )
	 )

  (define (removeMinValue prev curr)
      (cond
	((null? curr) nil); prev was leaf node?
	((null? (cdr curr));curr is leaf node, so set's parent's left child to nil 
	 (set-car! (cdr prev) nil)
	 ) 
	((and (null? (car (cdr curr))) (not (null? (cdr (cdr curr))))) ;curr has right child but not left
	 (if (> (car curr) (car prev)) ; curr is right child
	   (set-cdr! (cdr prev) (cdr (cdr curr)))
	   (set-car! (cdr prev) (cdr (cdr curr)))
	   )
	 )
	(else ;curr is not leaf node, so recur with curr's right child
	  (removeMinValue curr (car (cdr curr)))
	  )
	)
      )

  (define (delete val)
	(define (deleteHelper prevVal currVal)
	  (cond
		((null? currVal) nil)
		((eq? val (car currVal)) ;This is the value to be deleted. 3 cases, either leaf node, 1 child, or 2
		 (cond
		   ((null? (cdr currVal)) ; is a leaf node
			(cond 
			  ((> (car currVal) (car prevVal))
			  	(set-cdr! (cdr prevVal) nil)
				)
			  (else
			  	(set-car! (cdr prevVal) nil)
				)
			  )
			)
		   ((null? (car (cdr currVal))) ; does not have a left child
			(cond 
			  ((> (car currVal) (car prevVal))
			   (set-cdr! (cdr prevVal) (cdr (cdr currVal))) ;sets the right child of prev to right child
			   )
			  (else
				(set-car! (cdr prevVal) (cdr (cdr currVal))) ;sets the left child of prev to right child
				)
			  )
			)
		   ((null? (cdr (cdr currVal))) ; does not have a right child
			(cond
			  ((> (car currVal) (car prevVal))
			   (set-cdr! (cdr prevVal) (car (cdr currVal))) ;sets the right child of prev to left child
			   )
			  (else
				(set-car! (cdr prevVal) (car (cdr currVal))) ;sets the left child of prev to left child
				)
			  )
			)
		   (else ;has 2 children
			 (let ((tmp (findMinValue (cdr (cdr currVal)))))
			   (inspect tmp)
			   (removeMinValue currVal (cdr (cdr currVal)))
			   (set-car! currVal tmp)
			   )
			 )
		   )
		 )
		((> val (car currVal)); current value is greater than one to be deleted
		  (deleteHelper currVal (cdr (cdr currVal)))
		)
		(else
		  (deleteHelper currVal (car (cdr currVal)))
		  )
		)
	)
	(deleteHelper nil bstRoot)
)	
this
)

(define (flatten items)
  (cond
	((null? items) '())
	((not (pair? items)) (list items))
	(else
	  (append
		(flatten (car items)) (flatten (cdr items))
		)
	  )
	)
  )


;**********************************************************************************************************
; Problem 4
;**********************************************************************************************************
;QUEUE IMPLEMENTATION
(define (front-ptr queue) (car queue))
(define (rear-ptr queue) (cdr queue))
(define (set-front-ptr! queue item) (set-car! queue item))
(define (set-rear-ptr! queue item) (set-cdr! queue item))

(define (empty-queue? queue) (null? (front-ptr queue)))
(define (make-queue) (cons '() '()))
(define (front-queue queue)
  (if (empty-queue? queue)
    (error "FRONT called with an empty queue" queue)
    (car (front-ptr queue))))
(define (insert-queue! queue item)
  (let ((new-pair (cons item '())))
    (cond
      ((empty-queue? queue)
       (set-front-ptr! queue new-pair)
       (set-rear-ptr! queue new-pair)
       queue)
      (else
	(set-cdr! (rear-ptr queue) new-pair)
	(set-rear-ptr! queue new-pair)
	queue))))
(define (delete-queue! queue)
  (cond
    ((empty-queue? queue)
     (error "DELETE! called with an empty queue" queue))
    (else
      (set-front-ptr! queue (cdr (front-ptr queue)))
      queue)))





(define (nand-gate input1 input2 output)
  (define (nand-gate-action-procedure)
    (cond
      ((and (= (get-signal input1) 1) (= (get-signal input2) 1)) ; output is 0
       (after-delay nand-gate-delay
		    (lambda ()
		      (set-signal! output 0)
		      )
		    )
       )
      (else ;output is 1
	(after-delay nand-gate-delay
		     (lambda ()
		       (set-signal! output 1)
		       )
		     )
	)
      )
    )
  (add-action! input1 nand-gate-action-procedure)
  (add-action! input2 nand-gate-action-procedure)
  'ok
  )

(define (xor-gate a1 a2 output)
  (define (xor-action-procedure)
    (let ((c (make-wire))
	  (d (make-wire))
	  (e (make-wire))
	     )
	  (nand-gate a1 a2 c) ;c is nand gate of a1 and a2
	  (nand-gate a1 c d) ;d is nand gate of a1 and c
	  (nand-gate a2 c e) ;e is nand gate of a2 and c
	  (nand-gate d e output) ;output is nand gate of d and e
	  )
    )
  (add-action! a1 xor-action-procedure)
  (add-action! a2 xor-action-procedure)
  'ok
  )

(define (xnor-gate a1 a2 output)
  (define (xnor-action-procedure)
    (let ((c (make-wire))
	  (d (make-wire))
	  (e (make-wire))
	  (f (make-wire))
	  )
      (nand-gate a1 a2 c) ;c is the nand gate of a1 and a2
      (nand-gate a1 c d) ;d is the nand gate of a1 and c
      (nand-gate a2 c e) ;e is the nand gate of a2 and c
      (nand-gate d e f) ;f is the nand-gate of d and e
      (nand-gate f f output) ;the ouput is the nand gate of f and f
      )
    )
  (add-action! a1 xnor-action-procedure)
  (add-action! a2 xnor-action-procedure)
  'ok
  )
(define (call-each procedures)
  (if (null? procedures)
    'done
    (begin
      ((car procedures))
      (call-each (cdr procedures))
      )
    )
  )
(define (get-signal wire)
  (wire 'get-signal)
  )
(define (set-signal! wire new-value)
  ((wire 'set-signal!) new-value)
  )
(define (add-action! wire action-procedure)
  ((wire 'add-action!) action-procedure)
  )

(define (make-wire)
  (let ((signal-value 0) (action-procedures '()))
	(define (set-my-signal! new-value)
	  (if (not (= signal-value new-value))
		(begin (set! signal-value new-value)
			   (call-each action-procedures))
		'done))
	(define (accept-action-procedure! proc)
	  (set! action-procedures (cons proc action-procedures))
	  (proc))
	(define (dispatch m)
	  (cond
		((eq? m 'get-signal) signal-value)
		((eq? m 'set-signal!) set-my-signal!)
		((eq? m 'add-action!) accept-action-procedure!)
		(else
		  (error "Unknown operation -- WIRE" m))))
	  dispatch)
  )

(define (after-delay delayTime action)
  (add-to-agenda! (+ delayTime (current-time the-agenda))
		  action
		  the-agenda)
  )

(define (propagate)
  (if (empty-agenda? the-agenda)
    'done
    (let ((first-item (first-agenda-item the-agenda)))
      (first-item)
      (remove-first-agenda-item! the-agenda)
      (propagate)
      )
    )
  )

(define (probe name wire)
  (add-action! wire
	       (lambda ()
		 (newline)
		 (display name)
		 (display " ")
		 (display (current-time the-agenda))
		 (display " New-value = ")
		 (display (get-signal wire))
		 )
	       )
  )
(define (make-time-segment time queue)
  (cons time queue)
  )
(define (segment-time s) (car s))
(define (segment-queue s) (cdr s))
(define (make-agenda) (list 0))
(define (current-time agenda) (car agenda))
(define (set-current-time! agenda time)
  (set-car! agenda time)
  )
(define (segments agenda) (cdr agenda))
(define (set-segments! agenda segments)
  (set-cdr! agenda segments)
  )
(define (first-segment agenda) (car (segments agenda)))
(define (rest-segment agenda) (cdr (segments agenda)))
(define (empty-agenda? agenda)
  (null? (segments agenda)))

(define (add-to-agenda! time action agenda)
  (define (belongs-before? segments)
    (or (null? segments)
	(< time (segment-time (car segments)))))
  (define (make-new-time-segment time action)
    (let ((q (make-queue)))
      (insert-queue! q action)
      (make-time-segment time q)))
  (define (add-to-segments! segments)
    (if (= (segment-time (car segments)) time)
      (insert-queue! (segment-queue (car segments))
		     action)
      (let ((rest (cdr segments)))
	(if (belongs-before? rest)
	  (set-cdr! segments (cons (make-new-time-segment time action) (cdr segments)))
	  (add-to-segments! rest)))))
  (let ((segments (segments agenda)))
    (if (belongs-before? segments)
      (set-segments! agenda (cons (make-new-time-segment time action) segments))
      (add-to-segments! segments)))
  )

(define (remove-first-agenda-item! agenda)
  (let ((q (segment-queue (first-segment agenda))))
    (delete-queue! q)
    (if (empty-queue? q)
      (set-segments! agenda (rest-segment agenda)))))

(define (first-agenda-item agenda)
  (if (empty-agenda? agenda)
    (error "Agenda is empty -- FIRST-AGENDA-ITEM")
	   (let ((first-seg (first-segment agenda)))
	     (set-current-time! agenda (segment-time first-seg))
	     (front-queue (segment-queue first-seg)))))
(define the-agenda (make-agenda))
(define nand-gate-delay 6)

(define (run4)
(define input1 (make-wire))
(define input2 (make-wire))
(define nandOutput (make-wire))
(define xorOutput (make-wire))
(define xnorOutput (make-wire))


(nand-gate input1 input2 nandOutput)
(xor-gate input1 input2 xorOutput)
(xnor-gate input1 input2 xnorOutput)

(print "nand: 0 0 -> " )
(set-signal! input1 0)
(set-signal! input2 0)
(propagate)
(println (get-signal nandOutput))
(println "	[it should be 1]")
(print "xor: 0 0 -> " )
(set-signal! input1 0)
(set-signal! input2 0)
(propagate)
(println (get-signal xorOutput))
(println "	[it should be 0]")
(print "xnor: 0 0 -> ")
(set-signal! input1 0)
(set-signal! input2 0)
(propagate)
(println (get-signal xnorOutput))
(println "	[it should be 1]")
(println)

)
;************************************************************************************************************
;Problem 5
;************************************************************************************************************
;{
;(define (run5)
  )
;}

(define (mmutex size)
  )
;************************************************************************************************************
;Problem 6
;************************************************************************************************************
(define (run6)
  (show-stream pf-3-11-17 10)
  )
(define (merge stream1 stream2)
  (cond
	((stream-null? stream1) stream2)
	((stream-null? stream2) stream1)
	(else
	  (let ((s1car (stream-car stream1))
			(s2car (stream-car stream2))
			)
		(cond
		  ((< s1car s2car)
		   (cons-stream s1car (merge (stream-cdr stream1) stream2))
		   )
		  ((> s1car s2car)
		   (cons-stream s2car (merge stream1 (stream-cdr stream2)))
		   )
		  (else
			(cons-stream s1car (merge (stream-cdr stream1) (stream-cdr stream2)))
			)
		  )
		)
	  )
	)
  )

(define (scale-stream stream factor)
  (cons-stream 
	(* (stream-car stream) factor)
	(scale-stream (stream-cdr stream) factor)
	)
  )

(define getFactorStream
  (cons-stream
	1
	(merge (scale-stream getFactorStream 3)
		   (merge (scale-stream getFactorStream 11)
				  (scale-stream getFactorStream 17)
				  )
		   )
	)
  )

(define pf-3-11-17
  (stream-cdr getFactorStream)
  )
;************************************************************************************************************
;Problem 7
;************************************************************************************************************
;{
;(define (run7)
  (show-stream poly 10)
  (show-stream intPoly 10)
  )
;}
(define (signal f x dx)
  (cons-stream (f x) (signal f (+ x dx) dx))
  )
(define (integral stream dx)
  (cons-stream
    (* (stream-car stream) dx)
    (integral (stream-cdr stream) dx)
    )
  )

(define poly (signal (lambda (x) (+ (* x x) (* 3 x) -4)) 0 1))
(define intPoly (integral poly 1))
(define divIntPoly 0)
(define difference 0)
;************************************************************************************************************
;Problem 8
;************************************************************************************************************
;{
;(define (run8)
  (show-stream (ari 1) 10)
  (show-stream  ari-ps 10)
  (show-stream ari-acc 10)
  (show-stream ari-super 10)
  )
;}
(define (ari n)
  (cons-stream (/ 1.0 (^ 4 n))
			   (ari (+ n 1))
			   )
  )
(define (partial-sum stream)
  (cons-stream (stream-car stream)
			   (add-stream (stream-cdr stream)
							(partial-sum stream))
			   )
  )
(define (add-stream s1 s2)
  (cons-stream
	(+ (stream-car s1) (stream-car s2))
	(add-stream (stream-cdr s1) (stream-cdr s2))
	)
  )

(define (stream-ref stream n)
  (if (= n 0)
	(stream-car stream)
	(stream-ref (stream-cdr stream) (- n 1))
	)
  )
(define (stream-map proc stream)
  (if (stream-null? stream)
	the-empty-stream
	(cons-stream (proc (stream-car stream))
				 (stream-map proc (stream-cdr stream)
							 )
				 )
	)
  )
(define (euler-transform stream)
  (let ((s0 (stream-ref stream 0)) ;s(n-1)
		(s1 (stream-ref stream 1)) ;s(n)
		(s2 (stream-ref stream 2)) ;s(n+1)
		)
	(cond
	  ((= 0 (+ s0 (* -2 s1) s2))
	   (cons-stream s2 (euler-transform (stream-cdr stream))))
	  (else
		(cons-stream 
		  (- s2 (/ (* (- s2 s1) (- s2 s1))
				   (+ s0 (* -2 s1) s2)))
		  (euler-transform (stream-cdr stream))
		  )
		)
	  )
	)
  )

(define (make-tableau transform stream)
  (cons-stream stream
			   (make-tableau transform (transform stream))
			   )
  )

(define (acceleratedSequence transform stream)
  (stream-map stream-car (make-tableau transform stream)))

(define ari-ps (partial-sum (ari 1)))
(define ari-acc (euler-transform ari-ps))
(define ari-super (acceleratedSequence euler-transform ari-ps))
;************************************************************************************************************
;Problem 9
;************************************************************************************************************
(define (run9)
  (show-stream (ramanujan) 5)
  )
(define ones (cons-stream 1 ones))
(define integers (cons-stream 1 (add-stream ones integers)))
(define (stream-map proc stream)
  (if (stream-null? stream)
	the-empty-stream
	(cons-stream (proc (stream-car stream))
				 (stream-map proc (stream-cdr stream)
							 )
				 )
	)
  )
(define (add-stream stream1 stream2)
  (cons-stream
	(+ (stream-car stream1) (stream-car stream2))
	(add-stream (stream-cdr stream1) (stream-cdr stream2))
	)
  )
(define (stream-for-each proc stream)
  (if (stream-null? stream)
	'done
	(begin 
	  (proc (stream-car stream))
	  (stream-for-each proc (stream-cdr stream))
	  )
	)
  )
(define (show-stream stream numTimes)
  (if (= 0 numTimes)
	(println "done")
	(begin
	  (println (stream-car stream))
	  (show-stream (stream-cdr stream) (- numTimes 1))
	  )
	)
  )

(define (mergeWeightedStreams s1 s2 weight)
  (cond
	((stream-null? s1) s2)
	((stream-null? s2) s1)
	(else
	  (let ((s1car (stream-car s1))
			(s2car (stream-car s2)))
		(if (<= (weight s1car) (weight s2car))
		  (cons-stream s1car
					   (mergeWeightedStreams (stream-cdr s1)
											 s2
											 weight)
					   )
		  (cons-stream s2car
					   (mergeWeightedStreams s1
											 (stream-cdr s2)
											 weight)
					   )
		  )
		)
	  )
	)
  )

(define (getWeightedPairs stream1 stream2 weight)
  (cons-stream
	(list (stream-car stream1) (stream-car stream2))
	(mergeWeightedStreams
	  (stream-map (lambda (x) (list (stream-car stream1) x))
					(stream-cdr stream2)
					)
	  (getWeightedPairs (stream-cdr stream1) (stream-cdr stream2) weight)
	  weight
	  )
	)
  )

(define (ramanujan)
  (define (sumOfCubes x)
	(let ((i (car x)) (j (cadr x)))
	  (+ (* i i i) (* j j j)
		 )
	  )
	)
  (define (ramanujanStream sumOfCubesStream)
	(let ((current (stream-car sumOfCubesStream))
		  (next (stream-car (stream-cdr sumOfCubesStream)))
		  (possRamanujan (sumOfCubes (stream-car sumOfCubesStream)))
		  )
	  (cond
		((= possRamanujan (sumOfCubes next))
		 (cons-stream possRamanujan
					  (ramanujanStream (stream-cdr (stream-cdr sumOfCubesStream)))))
		(else
		  (ramanujanStream (stream-cdr sumOfCubesStream))
		  )
		)
	  )
	)
	  (ramanujanStream (getWeightedPairs integers integers sumOfCubes))
	  )

;************************************************************************************************************
;Problem 10
;************************************************************************************************************
(define (run10)
  )
(define (rgen inputStream)
  (define (rgen-iter seed commands)
  (if (stream-null? commands)
	null
	(let ((command (stream-car commands)))
	  (cond
		((eq? 'generate command)
		 (let ((newRand (rand-update seed)))
		   (cons-stream
			 newRand
			 (rgen-iter newRand (stream-cdr commands))
			 )
		   )
		 )
		((eq? 'reset command)
		 (let ((newRand (stream-car (stream-cdr commands))))
		   (cons-stream
			 newRand
			 (rgen-iter newRand (stream-cdr (stream-cdr commands)))
			 )
		   )
		 )
		(else
		  (error "INVALID INPUT" command)
		  )
		)
	  )
	)
  )
  (rgen-iter 0 inputStream)
  )

(define (rand-update x)
  (% (/ (* x x) 1000) 1000000)
  )

;(run1) DONE
;(run2)
;(run3) DONE
;(run4) DONE
;(run5)
;(run6) DONE
;(run7) INTEGRAL FUNCT???
;(run8) DONE but need to include def
;(run9) DONE
;(run10) DONE
(display "\n\nassignment 3 loaded!\n\n")
